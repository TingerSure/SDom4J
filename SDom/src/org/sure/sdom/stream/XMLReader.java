package org.sure.sdom.stream;

import java.io.BufferedReader;
import java.io.IOException;

public class XMLReader {

	/**
	 * 文本输入缓冲流
	 */
	private BufferedReader input = null;

	/**
	 * 文本缓冲区
	 */
	private StringBuilder builder = new StringBuilder();

	/**
	 * 构造方法
	 * 
	 * @param input
	 *            XML文件对应的缓冲输入流。
	 */
	public XMLReader(BufferedReader input) {
		this.input = input;
	}

	/**
	 * 返回下一个标签字符串
	 * 
	 * @return 下一个标签字符串
	 * @throws IOException
	 */
	public synchronized String next() throws IOException {
		boolean read = true;
		boolean text = false;
		do {
			if (builder.toString().startsWith("<!--")) {
				text = false;
				if (builder.indexOf("-->") == -1) {
					if (!builderRead()) {
						read = false;
						break;
					}
					read = true;
				} else {
					read = false;
				}
			} else if (builder.toString().startsWith("<")) {
				text = false;
				if (builder.indexOf(">") == -1) {
					if (!builderRead()) {
						read = false;
						break;
					}
					read = true;
				} else {
					read = false;
				}
			} else {
				text = true;
				if (builder.indexOf("<") == -1) {
					if (!builderRead()) {
						read = false;
						break;
					}
					read = true;
				} else {
					read = false;
				}
			}
		} while (read);
		String temp = null;
		if (text) {
			if (builder.indexOf("<") == -1) {
				temp = builder.toString();
				builder.delete(0, builder.length());
				if (temp.length() == 0) {
					return null;
				}
			} else {
				temp = builder.substring(0, builder.indexOf("<"));
				builder.delete(0, builder.indexOf("<"));
			}
		} else {
			if (builder.toString().startsWith("<!--")) {
				temp = builder.substring(0, builder.indexOf("-->") + 3);
				builder.delete(0, builder.indexOf("-->") + 3);
			} else {
				temp = builder.substring(0, builder.indexOf(">") + 1);
				builder.delete(0, builder.indexOf(">") + 1);
			}
		}
		if (temp.matches("\\s*")) {
			temp = next();
		}
		return temp;
	}

	/**
	 * 读一段数据
	 * 
	 * @return 是否还有数据
	 * @throws IOException
	 */
	private boolean builderRead() throws IOException {
		String buffer = input.readLine();
		if (buffer == null) {
			return false;
		}
		builder.append(buffer.trim() + "\n");
		return true;
	}

	/**
	 * 关闭该输入流
	 * 
	 * @throws IOException
	 */
	public synchronized void close() throws IOException {
		if (input != null) {
			input.close();
		}
		input = null;
	}

}
