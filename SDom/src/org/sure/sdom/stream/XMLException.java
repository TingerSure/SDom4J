package org.sure.sdom.stream;

public class XMLException extends Exception {

	private static final long serialVersionUID = 1L;

	public XMLException() {
		super();
	}

	public XMLException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public XMLException(String message, Throwable cause) {
		super(message, cause);

	}

	public XMLException(String message) {
		super(message);

	}

	public XMLException(Throwable cause) {
		super(cause);

	}

}
