package org.sure.sdom.document;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.sure.sdom.stream.XMLException;
import org.sure.sdom.stream.XMLReader;

/**
 * 
 * 文档树对象
 * 
 * @author TingerSure
 * 
 */
public class Document {

	/**
	 * 树结构根元素
	 */
	Element domRoot = null;

	/**
	 * 返回文档树形结构的根元素。<br>
	 * 如果该文档尚未载入文件，则返回null。
	 * 
	 * @return 文档树形结构的根元素
	 */
	public Element getRoot() {
		return domRoot;
	}

	/**
	 * 树构造栈
	 */
	protected Stack<Element> stack = null;

	/**
	 * 构造方法。<br>
	 * 通常在构造完对象后应该通过loadXML()方法载入一个xml文件。
	 */
	public Document() {

	}

	/**
	 * 根据id获得文档树结构中第一个id属性匹配的元素。
	 * 
	 * @param id
	 *            匹配的值
	 * @return 匹配的元素
	 */
	public Element getElementById(String id) {
		return domRoot.getElementById(id);
	}

	/**
	 * 根据路径获得元素。如果元素不存在则返回null。
	 * 
	 * @param path
	 *            路径
	 * @return 路径对应的元素
	 */
	public Element getElementByPath(String path) {
		Path pa = new Path(path, domRoot, false, false);
		return pa.getTarget();

	}

	/**
	 * 根据路径获得元素。如果元素不存在则返回null。
	 * 
	 * @param pa
	 *            路径对象
	 * @return 路径对应的元素
	 */
	public static Element getElementByPath(Path pa) {
		return pa.getTarget();
	}

	/**
	 * 根据name获得文档树结构中所有name属性匹配的元素。<br>
	 * 这些元素将以List的形式被返回。
	 * 
	 * @param name
	 *            匹配的值
	 * @return 匹配的元素的集合
	 */
	public List<Element> getElementsByName(String name) {
		List<Element> nameElement = new ArrayList<Element>();
		domRoot.getElementsByName(name, nameElement);
		return nameElement;
	}

	/**
	 * 返回描述该document的元素树结构的文本。<br>
	 * 此文本中每个元素的缩进情况可能会由于其setLevel()方法被调用而改变。
	 */
	@Override
	public String toString() {
		return domRoot.getInnerXML();
	}

	/**
	 * 向给定的字符流中写入这个docment 的文件内容
	 * 
	 * @param xml
	 *            文件流
	 * @throws IOException
	 */
	public void saveXML(BufferedWriter xml) throws IOException {
		try {
			xml.write(this.toString());
		} catch (IOException e) {
			throw e;
		} finally {
			if (xml != null) {
				xml.flush();
				xml.close();
			}
		}
	}

	/**
	 * xml读入流
	 */
	protected XMLReader xr;

	/**
	 * 载入XML文件
	 * 
	 * @param xml
	 *            要载入的xml文件的流
	 * @return 这个文档本身
	 * @throws IOException
	 * @throws XMLException
	 */
	public Document loadXML(BufferedReader xml) throws IOException,
			XMLException {

		domRoot = new Element().put("<root>", this);
		domRoot.setLevel(-1);
		stack = new Stack<Element>();
		stack.push(domRoot);
		try {
			xr = new XMLReader(xml);
			String temp = null;
			while ((temp = xr.next()) != null) {
				if (temp.startsWith("</")) {
					Element e = null;
					try {
						e = stack.peek();
					} catch (RuntimeException ex) {
						throw new XMLException("xml文件标签错误： " + temp);
					}
					if (!e.getName().equals(
							temp.substring(2, temp.length() - 1))) {
						throw new XMLException("xml文件标签错误： " + e.getName()
								+ temp);
					}
					stack.pop().setClose(true);
					continue;
				}
				if (temp.startsWith("<?") || temp.startsWith("<!")) {
					stack.peek().setEndNote(stack.peek().getEndNote() + temp);
					continue;
				}
				Element el = new Element();
				el.setNote(stack.peek().getEndNote());
				stack.peek().setEndNote("");
				stack.peek().addChildElement(el);
				el.setLevel(stack.size() - 1);
				el.put(temp, this);
				if (!el.isClose()) {
					stack.push(el);
				}
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (xml != null) {
				xml.close();
			}
		}
		return this;
	}

	/**
	 * 载入XML文件
	 * 
	 * @param xml
	 *            要载入的xml文件
	 * @return 这个文档本身
	 * @throws IOException
	 * @throws XMLException
	 */
	public Document loadXML(File xml) throws IOException, XMLException {
		return loadXML(new BufferedReader(new FileReader(xml)));
	}

	/**
	 * 载入XML文件
	 * 
	 * @param xml
	 *            要载入的xml文件名
	 * @return 这个文档本身
	 * @throws IOException
	 * @throws XMLException
	 */
	public Document loadXML(String xmlFile) throws IOException, XMLException {
		return loadXML(new BufferedReader(new FileReader(xmlFile)));
	}
}
