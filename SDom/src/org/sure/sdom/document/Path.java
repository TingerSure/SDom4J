package org.sure.sdom.document;

/**
 * 访问路径
 * 
 * @author TingerSure
 * 
 */
public class Path {

	/**
	 * 是否缓存查找结果。
	 */
	private boolean cache = false;

	/**
	 * 已经加载？
	 */
	private boolean havelookfor = false;

	/**
	 * 是否懒加载
	 */
	private boolean lazy = true;

	/**
	 * 路径
	 */
	private String path = null;

	/**
	 * 根元素
	 */
	private Element root = null;

	/**
	 * 目标元素
	 */
	private Element target = null;

	/**
	 * 构造一个路径对象
	 * 
	 * @param path
	 *            路径
	 * @param root
	 *            根元素
	 */
	public Path(String path, Element root) {
		this.path = path;
		this.root = root;
		pathFormate();
	}

	/**
	 * 构造一个路径对象
	 * 
	 * @param path
	 *            路径
	 * @param root
	 *            根元素
	 * @param cache
	 *            是否缓存查找结果
	 * @param lazy
	 *            是否懒加载
	 */
	public Path(String path, Element root, boolean cache, boolean lazy) {
		this.path = path;
		this.root = root;
		this.lazy = lazy;
		this.cache = cache;
		pathFormate();
		if (!this.lazy) {
			lookfor();
		}
	}

	/**
	 * 获得路径
	 * 
	 * @return 路径
	 */
	public String getPath() {
		return path;
	}

	/**
	 * 获得根元素
	 * 
	 * @return 根元素
	 */
	public Element getRoot() {
		return root;
	}

	/**
	 * 获得目标元素
	 * 
	 * @return 目标元素
	 */
	public Element getTarget() {
		if (!cache || (lazy && !havelookfor)) {
			lookfor();
		}
		return target;
	}

	/**
	 * 是否缓存
	 * 
	 * @return 是否缓存
	 */
	public boolean isCache() {
		return cache;
	}

	/**
	 * 是否懒加载
	 * 
	 * @return 是否懒加载
	 */
	public boolean isLazy() {
		return lazy;
	}

	/**
	 * 设置是否缓存
	 * 
	 * @param cache
	 *            缓存
	 */
	public void setCache(boolean cache) {
		this.cache = cache;
	}

	/**
	 * 设置是否懒加载
	 * 
	 * @param lazy
	 *            是否懒加载
	 */
	public void setLazy(boolean lazy) {
		if (this.lazy != lazy) {
			this.lazy = lazy;
			if (!this.lazy && !this.havelookfor) {
				lookfor();
			}
		}
	}

	/**
	 * 设置路径
	 * 
	 * @param path
	 *            路径
	 */
	public void setPath(String path) {
		if (path == null) {
			this.path = null;
			this.target = null;
		} else if (!path.equals(this.path)) {
			havelookfor = false;
			this.path = path;
			pathFormate();
			if (!lazy) {
				lookfor();
			}
		}
	}

	/**
	 * 设置根元素
	 * 
	 * @param root
	 *            根元素
	 */
	public void setRoot(Element root) {
		if (this.root != root) {
			this.havelookfor = false;
			this.root = root;
			this.target = null;
			if (!lazy) {
				lookfor();
			}
		}
	}

	private void pathFormate() {
		if (path == null) {
			return;
		}
		if (path.equals("/")) {
			return;
		}
		if (path.startsWith("/")) {
			path = path.substring(1);
		}
		if (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
	}

	/**
	 * 执行查找。
	 */
	public void lookfor() {
		havelookfor = true;
		path = path.trim();
		if (this.path == null) {
			target = null;
			return;
		}
		if (this.path.equals("/")) {
			target = root;
			return;
		}
		String[] route = path.split("/");
		Element temp = root;
		try {
			for (int i = 0; i < route.length; i++) {
				temp = getElementByRoute(route[i], temp);
			}
		} catch (RuntimeException npe) {
			target = null;
			return;
		}
		target = temp;
	}

	/**
	 * 根据线路查找元素<br>
	 * # id查找器<br>
	 * ! 序列查找器，(从0开始)<br>
	 * . 属性查找器， <br>
	 * 其他 标签名查找器<br>
	 * 注：单独的点表示当前元素的父元素
	 * 
	 * @param route
	 *            线路
	 * @param el
	 *            当前元素
	 * @return 目标元素
	 */
	private Element getElementByRoute(String route, Element el) {
		if (el == null || route == null || route.equals("")) {
			throw new NullPointerException();
		}
		if (route.equals(".")) {
			return el.getParentElement();
		}
		if (route.startsWith("#")) {
			return el.getChildElementById(route.substring(1));
		}
		if (route.startsWith("!")) {
			return el.getChildElement(Integer.parseInt(route.substring(1)));
		}
		if (route.startsWith(".")) {
			String[] attr = route.substring(1).split("=");
			return el.getChildElementByAttr(attr[0], attr[1]);
		}
		return el.getChildElement(route);
	}
}
