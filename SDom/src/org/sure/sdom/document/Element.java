package org.sure.sdom.document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 标签元素对象
 * 
 * @author TingerSure
 * 
 */
public class Element {
	/**
	 * 标签名
	 */
	private String name = null;
	/**
	 * 层级
	 */
	private int level = 0;

	/**
	 * 文本（如不是文本则为null）
	 */

	private String text = null;
	/**
	 * 属性列表
	 */
	private Map<String, String> attributes = new HashMap<String, String>();
	/**
	 * 子标签列表
	 */
	private List<Element> childElements = new ArrayList<Element>();

	/**
	 * 父标签
	 */
	private Element parentElement = null;

	/**
	 * 标签是否已关闭
	 */
	private boolean close = false;

	/**
	 * 头注释
	 */
	String note = null;

	/**
	 * 尾注释
	 */
	String endNote = "";

	/**
	 * 构造函数
	 */
	public Element() {

	}

	/**
	 * dom树
	 */
	Document doc = null;

	/**
	 * 将文本解析为标签，如果不是标签则直接作为文本保存
	 * 
	 * @param e
	 *            要解析的文本
	 */

	Element put(String e, Document doc) {
		this.doc = doc;
		e = e.trim();
		// 文本
		if (!e.startsWith("<")) {
			this.text = e;
			this.close = true;
			return this;
		}
		// 非文本
		if (e.matches("^<[^\\s]*>$")) {
			this.name = e.substring(1, e.length() - 1);
			this.close = false;
			return this;
		}
		e = formate(e.replaceAll("[\\s]+", " "));
		String[] temp = e.split(" ");
		this.name = temp[0].substring(1);
		this.close = temp[temp.length - 1].endsWith("/>");
		for (int i = 1; i < temp.length - 1; i++) {
			String[] attr = temp[i].split("=");
			this.setAttribute(
					attr[0],
					attr[1].substring(1, attr[1].length() - 1)
							.replaceAll("&nbsp;", " ").replaceAll("&eq;", "=")
							.replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
		}
		return this;
	}

	/**
	 * 格式化标签文本。
	 * 
	 * @param e
	 *            要格式化的文本
	 * @return 格式化之后的文本
	 */
	private static String formate(String e) {
		int index = 0;
		for (int i = e.length() - 1; i > -1; i--) {
			if (e.charAt(i) == '"') {
				if (index % 2 == 0) {
					for (int j = i + 1; j < e.length(); j++) {
						if (e.charAt(j) != ' ') {
							e = e.substring(0, i + 1) + " " + e.substring(j);
							break;
						}
					}
				}
				index++;
			}
			if (e.charAt(i) == ' ') {
				if (index % 2 == 1) {
					e = e.substring(0, i) + "&nbsp;" + e.substring(i + 1);
				}
			}
			if (e.charAt(i) == '=') {
				if (index % 2 == 1) {
					e = e.substring(0, i) + "&eq;" + e.substring(i + 1);
				}
			}
			if (e.charAt(i) == '<') {
				if (index % 2 == 1) {
					e = e.substring(0, i) + "&lt;" + e.substring(i + 1);
				}
			}
			if (e.charAt(i) == '>') {
				if (index % 2 == 1) {
					e = e.substring(0, i) + "&gt;" + e.substring(i + 1);
				}
			}
		}
		e = e.replaceAll("\\s*=\\s*", "=");
		return e;
	}

	/**
	 * 返回标签名
	 * 
	 * @return 标签名
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置标签名
	 * 
	 * @param name
	 *            标签名
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 获得文本（如果是文本的话）
	 * 
	 * @return 文本内容
	 */
	public String getText() {
		return text;
	}

	/**
	 * 设置为文本 text
	 * 
	 * @param text
	 *            文本
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 是否是文本
	 * 
	 * @return 是否为文本
	 */
	public boolean isText() {
		return text != null;

	}

	/**
	 * 通过属性名获得属性值
	 * 
	 * @param key
	 *            属性名
	 * @return 属性值
	 */
	public String getAttribute(String key) {
		return attributes.get(key);
	}

	/**
	 * 以Map形式返回这个元素的所有属性，操作这个Map对象不会对这个元素本身造成影响
	 * 
	 * @return 所有属性的Map集合
	 */
	public Map<String, String> getAttributes() {
		return new HashMap<String, String>(attributes);
	}

	/**
	 * 以List方式返回这个元素的所有子元素，操作这个List不会对这个元素本身造成影响
	 * 
	 * @return 所有子元素的链表
	 */
	public List<Element> getChildElements() {
		return new ArrayList<Element>(childElements);
	}

	/**
	 * 设在属性值（如没有则被创建）
	 * 
	 * @param key
	 *            属性名
	 * @param value
	 *            属性值
	 */
	public void setAttribute(String key, String value) {
		attributes.put(key, value);
	}

	/**
	 * 根据索引获得子元素
	 * 
	 * @param index
	 *            索引
	 * @return 对应的元素
	 */
	public Element getChildElement(int index) {
		return childElements.get(index);
	}

	/**
	 * 查找该元素的第一个与给定标签名相匹配的子元素。
	 * 
	 * @param elementName
	 *            标签名
	 * @return 第一个匹配的元素
	 */
	public Element getChildElement(String elementName) {
		Element e = null;
		for (Iterator<Element> it = childElements.iterator(); it.hasNext();) {
			e = it.next();
			if (e.getName() == null) {
				continue;
			}
			if (e.getName().equals(elementName)) {
				return e;
			}
		}
		return null;
	}

	/**
	 * 获得子元素数量（包括文本）
	 * 
	 * @return 子元素数量
	 */
	public int getChildSize() {
		return childElements.size();
	}

	/**
	 * 添加一个子元素。
	 * 
	 * @param child
	 *            要添加的子元素
	 */
	public void addChildElement(Element child) {
		child.parentElement = this;
		this.childElements.add(child);
	}

	/**
	 * 向指定位置插入一个元素。
	 * 
	 * @param child
	 *            要插入的元素
	 * @param index
	 *            索引位置
	 */
	public void insertChildElement(Element child, int index) {
		child.parentElement = this;
		this.childElements.add(index, child);
	}

	/**
	 * 按索引移除一个子元素，返回这个元素
	 * 
	 * @param index
	 *            索引
	 * @return 这个元素
	 */
	public Element removeChildElement(int index) {
		Element e = childElements.remove(index);
		e.setParentElement(null);
		return e;
	}

	/**
	 * 移除指定的元素，返回是否成功
	 * 
	 * @param e
	 *            要移除的元素
	 * @return 移除是否成功
	 */
	public boolean removeChildElement(Element e) {
		e.setParentElement(null);
		return childElements.remove(e);
	}

	/**
	 * 获得父元素
	 * 
	 * @return 父元素
	 */
	public Element getParentElement() {
		return parentElement;
	}

	/**
	 * 设置父元素
	 * 
	 * @param parentElement
	 *            父元素
	 */
	void setParentElement(Element parentElement) {
		this.parentElement = parentElement;
	}

	/**
	 * 返回该标签是否已闭合。<br>
	 * 
	 * @return 是否已闭合
	 */
	boolean isClose() {
		return close;
	}

	/**
	 * 设置标签的闭合状态
	 * 
	 * @param close
	 *            是否闭合
	 */
	void setClose(boolean close) {
		this.close = close;
	}

	/**
	 * 返回元素以及子元素的树形结构的字符串表示
	 */
	@Override
	public String toString() {
		String toStr = "";
		if (!"".equals(this.note)) {
			for (int i = 0; i < level; i++) {
				toStr += "\t";
			}
			toStr += note + "\n";
		}
		for (int i = 0; i < level; i++) {
			toStr += "\t";
		}
		if (isText()) {
			toStr += text + "\n";
			return toStr;
		}
		toStr += "<" + name;
		Iterator<String> it = attributes.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			toStr += " " + key + "=\"" + attributes.get(key) + "\"";
		}
		if (childElements.size() != 0 || !"".equals(endNote)) {
			toStr += ">\n";
			for (Iterator<Element> its = childElements.iterator(); its
					.hasNext();) {
				toStr += its.next().toString();
			}
			if (!"".equals(this.endNote)) {
				for (int i = 0; i < level + 1; i++) {
					toStr += "\t";
				}
				toStr += endNote + "\n";
			}
			for (int i = 0; i < level; i++) {
				toStr += "\t";
			}

			toStr += "</" + name + ">\n";
		} else {
			toStr += "/>\n";
		}
		return toStr;
	}

	/**
	 * 返回元素的所有子元素的字符串表示。
	 * 
	 * @return 所有子元素的字符串表示
	 */
	public String getInnerXML() {
		String toStr = "";
		if (isText()) {
			return null;
		}
		if (childElements.size() != 0 || !"".equals(endNote)) {
			for (Iterator<Element> its = childElements.iterator(); its
					.hasNext();) {
				toStr += its.next().toString();
			}
			if (!"".equals(this.endNote)) {
				for (int i = 0; i < level + 1; i++) {
					toStr += "\t";
				}
				toStr += endNote + "\n";
			}
		}
		return toStr;
	}

	/**
	 * 获得该元素的层级
	 * 
	 * @return 层级
	 */
	int getLevel() {
		return level;
	}

	/**
	 * 设置该元素的层级，此方法不会真正改变元素的层级关系，只会影响此元素及其父元素的toString()方法。
	 * 
	 * @param level
	 *            层级
	 */
	void setLevel(int level) {
		this.level = level;
	}

	/**
	 * 根据id查找匹配的子元素
	 * 
	 * @param id
	 *            元素id
	 * @return 找到的元素
	 */
	public Element getChildElementById(String id) {
		if (this.isText()) {
			return null;
		}
		if (id == null) {
			return null;
		}
		Element temp;
		for (Iterator<Element> it = childElements.iterator(); it.hasNext();) {
			temp = it.next();
			if (id.equals(temp.getAttribute("id"))) {
				return temp;
			}
		}
		return null;
	}

	/**
	 * 根据属性查找匹配的子元素
	 * 
	 * @param id
	 *            元素id
	 * @return 找到的元素
	 */
	public Element getChildElementByAttr(String key, String value) {
		if (this.isText()) {
			return null;
		}
		if (key == null) {
			return null;
		}
		Element temp;
		for (Iterator<Element> it = childElements.iterator(); it.hasNext();) {
			temp = it.next();
			String va = temp.getAttribute(key);
			if ((value == null && va == null) || value.equals(va)) {
				return temp;
			}
		}
		return null;
	}

	/**
	 * 根据路径获得元素。如果元素不存在则返回null。
	 * 
	 * @param path
	 *            路径
	 * @return 路径对应的元素
	 */
	public Element getElementByPath(String path) {
		Path pa = new Path(path, this, false, false);
		return pa.getTarget();

	}

	/**
	 * 根据id获得以此元素作为根元素的元素树结构中第一个id值匹配的元素。
	 * 
	 * @param id
	 *            要寻找的id
	 * @return id值匹配的元素
	 */
	public Element getElementById(String id) {
		if (this.isText()) {
			return null;
		}
		if (id == null) {
			return null;
		}
		if (id.equals(this.getAttribute("id"))) {
			return this;
		}
		Element temp;
		for (Iterator<Element> it = childElements.iterator(); it.hasNext();) {
			if ((temp = it.next().getElementById(id)) != null) {
				return temp;
			}
		}
		return null;
	}

	/**
	 * 根据name获得以此元素作为根元素的元素树结构中所有name属性匹配的元素，<br>
	 * 匹配的元素将会被放入指定List中。
	 * 
	 * @param name
	 *            要寻找的name
	 * @param nameElement
	 *            指定用来放元素的List
	 */
	public void getElementsByName(String name, List<Element> nameElement) {
		if (this.isText()) {
			return;
		}
		if (name == null) {
			return;
		}
		if (name.equals(this.getAttribute("name"))) {
			nameElement.add(this);
		}
		for (Iterator<Element> it = nameElement.iterator(); it.hasNext();) {
			it.next().getElementsByName(name, nameElement);

		}

	}

	/**
	 * 获得该元素的第一个子元素的内容（如果存在并且为文本的话），如不存在或不是文本，则返回null。
	 * 
	 * @return 文本
	 */
	public String getChildText() {

		if (this.childElements.size() == 0) {
			return "";
		}
		if (this.childElements.get(0).isText()) {
			return this.childElements.get(0).getText();
		}
		return null;

	}

	/**
	 * 获得注释
	 * 
	 * @return 注释
	 */
	public String getNote() {
		return note;
	}

	/**
	 * 设置注释
	 * 
	 * @param note
	 *            注释
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * 获得无附属的注释
	 * 
	 * @return 注释
	 */
	public String getEndNote() {
		return endNote;
	}

	/**
	 * 设置无附属的注释
	 * 
	 * @param endNote
	 *            注释
	 */
	public void setEndNote(String endNote) {
		this.endNote = endNote;
	}

}
